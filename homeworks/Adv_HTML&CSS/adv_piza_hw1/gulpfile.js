'use strict';

const gulpfile = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');

sass.compiler = require('node-sass');

gulpfile.task('styles', function () {
    return gulpfile.src('scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulpfile.dest('css'));
});

gulpfile.task('watch', function () {
    gulpfile.watch('scss/*.scss', gulpfile.series('styles'));
});

// ======= //

// gulpfile.task('style', function (done) {
//     return gulpfile.src('scss/*.scss')
//         .pipe(sass().on('error', sass.logError))
//         .pipe(cleanCSS())
//         .pipe(gulpfile.dest('build/css'));
//
//     done();
// });
//
// gulpfile.task('style:watch', function () {
//     return gulpfile
//         .watch(
//             'scss/*.scss',
//             gulpfile.series('style')
//         );
// });

