const menu = document.querySelector('.button');
const div = document.querySelector('.vertical');
const cross = document.querySelector('.cross');
menu.onmouseover = function(){
    div.classList.add('active');
    cross.classList.remove('hidden');
};
menu.onmouseout = function(){
    div.classList.remove('active');
    cross.classList.add('hidden');
};
