class Hamburger {
    constructor (size, stuffing) {
        if (!size) {
            throw new  HamburgerException('Error! No size given');
        }
        if (!stuffing) {
            throw new HamburgerException('Error! No staffing given');
        }
        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];
    }
    static SIZE_SMALL = {
        price: 50,
        calories: 20
    };
    static  SIZE_LARGE = {
        price: 100,
        calories: 40
    };
    static STUFFING_CHEESE = {
        price: 10,
        calories: 20
    };
    static STUFFING_SALAD = {
        price: 20,
        calories: 5
    };
    static STUFFING_POTATO = {
        price: 15,
        calories: 10
    };
    static TOPPING_MAYO = {
        price: 20,
        calories: 5
    };
    static TOPPING_SPICE = {
        price: 15,
        calories: 0
    };

    getSize () {
        return this.size;
    }

    getStuffing () {
        return this.stuffing;
    }
    getToppings () {
        return this.toppings;
    }

     addTopping (topping) {
         for (let i = 0; i < this.toppings.length; i++){
             if (this.toppings[i] === topping) {
                 throw new HamburgerException("Duplicated topping");
             }
         }
         this.toppings.push(topping);
    }

    removeTopping(topping) {
        for (let i = 0; i < this.toppings.length; i++) {
            if (this.toppings[i] !== topping) {
                throw new HamburgerException("Not found such topping");
            } else {
                this.toppings.splice(i, 1);
            }
        }
    }
    calculatePrice () {
        let result = 0;
        result += this.size['price'];
        result += this.stuffing['price'];
        for(let i = 0; i < this.toppings.length; i++) {
            result += this.toppings[i]['price'];
        }
        return result;
    }
    calculateCalories () {
        let resultCal = 0;
        resultCal += this.size['calories'];
        resultCal += this.stuffing['calories'];
        for(let i = 0; i < this.toppings.length; i++) {
            resultCal += this.toppings[i]['calories'];
        }
        return resultCal;
    }
};
 function HamburgerException (message){
     this.message = message;
 };

try {
    const hamburger = new Hamburger(
        Hamburger.SIZE_SMALL,
        Hamburger.STUFFING_CHEESE
    );
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
    console.log(hamburger);
    console.log("Price: %f", hamburger.calculatePrice());
    console.log("Calories: %f", hamburger.calculateCalories());
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
    console.log("Price with sauce: %f", hamburger.calculatePrice());
    console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE);
    hamburger.removeTopping(Hamburger.TOPPING_MAYO);
    console.log("Have %d toppings", hamburger.getToppings().length);
}
catch (error) {
    console.log(error);
}


