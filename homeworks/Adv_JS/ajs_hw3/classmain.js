const circles =  document.getElementsByTagName('td');
const score = document.getElementById('score');
const start = document.getElementById('start');
let messageResult = document.getElementById('result');
let lastCircle;
let count = 0;
let countActive = 0;
let time;

class Game {
   static randomCircles(circles) {
            const idx = Math.floor(Math.random() * circles.length);
            const circle = circles[idx];
            if (circle === lastCircle) {
                return circle
            }
            lastCircle = circle;
            return circle;
    }
    static colored() {
        if (countActive > 50) {
            const message = 'Your score: '+ count + '  ' + (count >= 25 ? "Winner!" : "Loser!");
            messageResult.textContent = message;
            Game.clear();
        }
        const circle = Game.randomCircles(circles);
        circle.classList.add('active');
        countActive++;
        setTimeout(() => {
            circle.classList.add('red');
            circle.classList.remove('active');
            if (Game.colored()) return;
        }, time);
        for (let i = 0; i < circles.length; i++) {
            circles[i].onclick = (function () {
                if (circles[i].classList.contains('active')) {
                    count++;
                    circles[i].classList.add('green');
                    score.textContent = count;
                }
            });
        }
    }
    static clear (){
        setTimeout(
        function() {
            location.reload();
        }, 2000);
    }
}
start.addEventListener('click', function () {
    const checkLevel = function () {
        const btns = document.querySelectorAll('.btn');
        for(let i = 0; i < btns.length; i++){
            let item = btns[i];
            let attTime = item.getAttribute('data-time');
            if (item.classList.contains('active')) {
                time = attTime;
                start.style.backgroundColor = 'green';
                score.textContent = 0;
                messageResult.textContent = '';
                count = 0;
                Game.colored();
            }
        }
    };
    checkLevel();
    start.setAttribute('disabled', 'true')
});
let parent = document.querySelector('#parent');
parent.addEventListener('click', function (e) {
    if (e.target !== e.currentTarget){
        let clickeditem = e.target;
        clickeditem.style.backgroundColor = 'green';
        clickeditem.classList.add('active');
    }
});
