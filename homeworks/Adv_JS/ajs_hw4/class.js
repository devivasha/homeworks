class Card {
    static newElem () {
        const divFirst = document.createElement('div');
        divFirst.setAttribute('class', 'addColumn');
        divFirst.setAttribute('id', 'idAddColumn');
        document.body.appendChild(divFirst);
        const divTxt = document.createElement('span');
        divTxt.setAttribute('class', 'addColumnText');
        divTxt.setAttribute('id', 'idAddColumnText');
        divTxt.textContent = '+ Добавьте ещe одну колонку';
        divFirst.append(divTxt);
        divTxt.addEventListener('click', () => Card.newElemExpanded());
    }
    static newElemExpanded () {
        const div = document.getElementById('idAddColumn');
        const text = document.getElementById('idAddColumnText');
        const inputFirst = document.createElement('input');
        inputFirst.setAttribute('class', 'addColumnInput');
        inputFirst.setAttribute('id', 'idAddColumnInput');
        inputFirst.setAttribute('type', 'text');
        inputFirst.setAttribute('value', 'Ввести заголовок списка');
        inputFirst.addEventListener('click', function () {
            inputFirst.setAttribute('value', '');
        });
        div.prepend(inputFirst);
        const sbmButton = document.createElement('button');
        sbmButton.setAttribute('class', 'addColumnButton');
        sbmButton.setAttribute('id', 'idAddColumnButton');
        sbmButton.textContent = '+Добавить список';
        div.append(sbmButton);
        text.textContent = '';
        sbmButton.addEventListener('click', () => Card.createCard());
        sbmButton.onclick = function () {
            inputFirst.value = '';
        }
    }
      static createCard() {
                const valueChaced = document.getElementById('idAddColumnInput').value;
                if(valueChaced === '' || valueChaced === 'Ввести заголовок списка') return;
                const container = document.createElement('div');
                container.setAttribute('class', 'mainDiv');
                container.setAttribute('id', 'idMainDiv');
                document.body.appendChild(container);
                const txtCont = document.querySelector('.addColumnInput').value;
                const headers = document.createElement('div');
                headers.setAttribute('class', 'mainHeaders');
                headers.textContent = txtCont;
                container.append(headers);
                const noteContainer = document.createElement('div');
                noteContainer.setAttribute('class', 'divContainer');
                noteContainer.setAttribute('id', 'list');
                container.append(noteContainer);
                noteContainer.addEventListener('dragover', (e) => Card.dragOverTarget (e));
                noteContainer.addEventListener('drop', (e) => Card.dorpTarget(e));
                const sortT =  document.createElement('div');
                sortT.setAttribute('class', 'sortHeaders');
                sortT.textContent = '...';
                container.append(sortT);
                sortT.addEventListener('click', () => Card.sortButton(noteContainer));
                const inputS = document.createElement('input');
                inputS.setAttribute('class', 'inputInside');
                inputS.setAttribute('id', 'addInput');
                inputS.setAttribute('type', 'text');
                inputS.setAttribute('value', 'Ввести заголовок для этой карточки');
                container.append(inputS);
                inputS.addEventListener('click', function () {
                    inputS.setAttribute('value', '');
                });
                const button = document.createElement('div');
                button.setAttribute('class', 'buttonInside');
                button.setAttribute('id', 'idButtonInside');
                button.textContent = '+ Добавить карточку';
                container.append(button);
                button.addEventListener('click', () => Card.createNote(inputS, noteContainer));
                button.onclick = function () {
                inputS.value = '';
            }
        }
        static createNote(inputS, noteContainer) {
            if(inputS.value === '' || inputS.value === 'Ввести заголовок для этой карточки') return;
            let divCover = document.createElement('div');
            divCover.setAttribute('class', 'itemText');
            divCover.setAttribute('id', `note-item-${inputS.value}`);
            divCover.setAttribute('draggable', 'true');
            divCover.textContent = inputS.value;
            noteContainer.append(divCover);
            divCover.addEventListener('dragstart', (e) => Card.dragStTarget(e));
    }
       static sortButton (noteContainer) {
                let toSort = noteContainer.children;
                let newList = [];

                for (let z = 0; z < toSort.length; z++) {
                    newList.push(toSort[z].textContent);
                }

                let results = newList.sort(function compare(a, b) {
                    if (a < b) return -1;
                    if (a > b) return 1;
                    if (a === b) return 0;
                    return newList;
                });

                noteContainer.innerHTML = "";
                for (let i = 0; i < results.length; i++) {
                    let divCover = document.createElement('div');
                    divCover.setAttribute('class', 'itemText');
                    divCover.setAttribute('id', `note-item-${results[i]}`);
                    divCover.setAttribute('draggable', 'true');
                    divCover.textContent = results[i];
                    noteContainer.append(divCover);
                    divCover.addEventListener('dragstart', (e) => Card.dragStTarget(e));
                    }
        }

       static dragOverTarget (e) {
            e.preventDefault();
        }
       static dorpTarget (e) {
            e.preventDefault();
            const source = document.getElementById(`note-item-${e.dataTransfer.getData('srcId')}`);
            source.id = e.target.id;
            e.target.id = `note-item-${e.dataTransfer.getData('srcId')}`;
            source.innerHTML = e.target.innerHTML;
            e.target.innerHTML = e.dataTransfer.getData('srcId');
        }
       static dragStTarget(e) {
            e.dataTransfer.setData('srcId', e.target.innerHTML);
            }
}

Card.newElem();
