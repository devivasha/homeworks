window.onload = function ajaxGet() {
    const request = new XMLHttpRequest();
    request.onload = function(){
            let data = JSON.parse(request.responseText);
            let title = data.results;
            let ul = document.createElement('ul');
            let div = document.getElementById('film');
            div.appendChild(ul);
            title.map(function (elem){
                let li = document.createElement('li');
                let btn = document.createElement('button');
                btn.innerHTML = 'Load characters';
                btn.setAttribute('class', 'btn');
                li.innerHTML = `${elem.episode_id}`+ '<br/>' +  `${elem.title}` + '<br/>' + `${elem.opening_crawl}` + '<br/>';
                ul.appendChild(li);
                li.appendChild(btn);
                btn.addEventListener('click', function () {
                    btn.classList.add('active');
                    const characters = elem.characters;
                    console.log(characters);
                    const charLength = characters.length;
                    let characterNames = [];
                    characters.forEach(function (character) {
                       getData(character)
                            .then(res => {
                                characterNames.push(res.name);
                                if (characterNames.length === charLength) {
                                    let div = document.createElement('div');
                                    li.appendChild(div);
                                    div.innerHTML = characterNames.join(', ');
                                }
                            });
                    });

                })
            });
    };
    request.onerror = function(){
        console.log('Error');
    };
    request.open('GET', 'https://swapi.co/api/films/');
    request.send();
}

function getData(url) {
    return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest();
        request.open('GET', url);
        request.onload = function () {
                    const data = JSON.parse(this.response);
                    resolve(data);
        };
        request.onerror = function(){
            console.log('Error');
        };
        request.send();
    });
}
