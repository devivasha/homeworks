document.addEventListener('DOMContentLoaded', getUsers);
document.addEventListener('DOMContentLoaded', getPost);
document.addEventListener('DOMContentLoaded', renderPost);

function renderPost() {
    Ajax
        .get('https://jsonplaceholder.typicode.com/posts')
        .then(function (data) {
            if (Array.isArray(data)) {
                data.forEach(item => {
                    const main = document.getElementById('twitLine');
                    const templates = document.createElement('div');
                    templates.setAttribute('class', 'wrapper');
                    templates.setAttribute('id', `${item.id}`);
                    main.appendChild(templates);
                    const bgcover = document.createElement('div');
                    bgcover.setAttribute('class', 'cover');
                    bgcover.setAttribute('id', `${item.id}`);
                    templates.appendChild(bgcover);
                    const attTwit = document.createElement('div');
                    attTwit.setAttribute('class', 'attTwit_wrapper');
                    bgcover.appendChild(attTwit);
                    const pin = document.createElement('i');
                    pin.setAttribute('class', "fas fa-thumbtack");
                    attTwit.appendChild(pin);
                    const pintext = document.createElement('span');
                    pintext.setAttribute('class', 'pintext');
                    pintext.textContent = 'Закрепленный твит';
                    attTwit.appendChild(pintext);

                    const header = document.createElement('div');
                    header.setAttribute('class', 'header');
                    header.setAttribute('id', 'idHeader');
                    bgcover.appendChild(header);
                    const circle = document.createElement('img');
                    circle.setAttribute('class', 'circle');
                    circle.src = choosePic();
                    header.appendChild(circle);
                    const headerTitleCover = document.createElement('div');
                    headerTitleCover.setAttribute('class', 'title-cover');
                    header.appendChild(headerTitleCover);
                    const name = document.createElement('div');
                    name.setAttribute('class', 'name');
                    name.setAttribute('data-name',  `${item.userId}`);

                    const nameInput = document.createElement('div');
                    nameInput.setAttribute('class', 'nameInput');
                    name.appendChild(nameInput);
                    headerTitleCover.appendChild(name);
                    const tag = document.createElement('div');
                    tag.setAttribute('class', 'tag');
                    name.appendChild(tag);
                    const badge = document.createElement('i');
                    badge.setAttribute('class', 'fas fa-certificate');
                    tag.appendChild(badge);
                    const address = document.createElement('div');
                    address.setAttribute('class', 'address');
                    name.appendChild(address);
                    const text = document.createElement('div');
                    text.setAttribute('class', 'text');
                    text.textContent = item.title;
                    headerTitleCover.appendChild(text);

                    const arrow = document.createElement('i');
                    arrow.setAttribute('class', 'fas fa-angle-down fa-lg');
                    header.appendChild(arrow);

                    const bigPicWrapper = document.createElement('div');
                    bigPicWrapper.setAttribute('class', 'bigPic-wrapper');
                    bgcover.appendChild(bigPicWrapper);
                    const pic = document.createElement('img');
                    pic.setAttribute('class', 'pic');
                    pic.setAttribute('id', 'myPicture');
                    pic.src = choosePic();
                    bigPicWrapper.appendChild(pic);
                    const headerTitle = document.createElement('div');
                    headerTitle.setAttribute('class', 'headerTitle');
                    headerTitle.textContent = item.title;
                    bigPicWrapper.appendChild(headerTitle);
                    const publiction = document.createElement('div');
                    publiction.setAttribute('class', 'publiction');
                    publiction.textContent = item.body;
                    headerTitle.appendChild(publiction);
                    const email = document.createElement('div');
                    email.setAttribute('class', 'email');
                    headerTitle.appendChild(email);

                    const footer = document.createElement('div');
                    footer.setAttribute('class', 'footer');
                    bgcover.appendChild(footer);
                    const comment = document.createElement('div');
                    comment.setAttribute('class', 'comment');
                    footer.appendChild(comment);
                    const comIcon = document.createElement('i');
                    comIcon.setAttribute('class', 'far fa-comment');
                    comment.appendChild(comIcon);
                    const comData = document.createElement('div');
                    comData.setAttribute('class', 'comData');
                    comData.innerHTML = '10,7 тыс.';
                    comment.appendChild(comData);

                    const view = document.createElement('div');
                    view.setAttribute('class', 'view');
                    footer.appendChild(view);
                    const comArrow = document.createElement('i');
                    comArrow.setAttribute('class', 'fas fa-expand-arrows-alt');
                    view.appendChild(comArrow);
                    const viewData = document.createElement('div');
                    viewData.setAttribute('class', 'viewData');
                    viewData.innerHTML = '16,9 тыс.';
                    view.appendChild(viewData);

                    const heart = document.createElement('div');
                    heart.setAttribute('class', 'heart');
                    footer.appendChild(heart);
                    const heartIcon = document.createElement('i');
                    heartIcon.setAttribute('class', 'far fa-heart');
                    heart.appendChild(heartIcon);
                    const heartData = document.createElement('div');
                    heartData.setAttribute('class', 'heartData');
                    heartData.innerHTML = '20,9 тыс.';
                    heart.appendChild(heartData);

                    const upload = document.createElement('div');
                    upload.setAttribute('class', 'upload');
                    footer.appendChild(upload);
                    const uploadIcon = document.createElement('i');
                    uploadIcon.setAttribute('class', 'fas fa-upload');
                    upload.appendChild(uploadIcon);

                    const butns = document.createElement('div');
                    butns.setAttribute('class', 'btns');
                    header.appendChild(butns);
                    const cross = document.createElement('button');
                    cross.setAttribute('class', 'cross');
                    cross.innerHTML ='X';
                    cross.onclick = function () {
                        templates.classList.add('close');
                        axios.delete(`https://jsonplaceholder.typicode.com/posts/${item.id}`);
                    };
                    butns.appendChild(cross);
                    const change = document.createElement('button');
                    change.setAttribute('class', 'change');
                    butns.appendChild(change);
                    change.innerHTML = '&#9998';
                    change.onclick =  function () {
                        const modal = document.querySelector('.modal');
                        modal.classList.add('show');
                    };
                    const buttonAdd = document.querySelector('.post');
                    buttonAdd.onclick =  function () {
                        const modal = document.querySelector('.modal');
                        modal.classList.add('show');
                    };
                    const update = document.querySelector('.btn');
                    update.onclick =  function () {
                            return {
                                type: 'EDIT',
                                payload: new Promise((resolve, reject) => {
                                    const title = document.getElementById('title').value;
                                    const post = document.getElementById('post').value;
                                    let newPost = {
                                        title:title,
                                        body:post
                                    };
                                    arrayPosts.unshift(newPost);

                                    axios({
                                        method: 'put',
                                        url: 'https://jsonplaceholder.typicode.com/posts',
                                        data: newPost,
                                        config: { headers: {'Content-Type': 'multipart/form-data' }}
                                    })
                                        .then(function (response) {
                                            if (response.status === 200) {
                                                console.log("Update Success");
                                                resolve();
                                            }
                                        })
                                        .catch(function (response) {
                                            console.log(response);
                                            resolve();
                                        });
                                })
                            };
                    };

                    for(let i = 1; i < 11; i++){
                        if(item.userId === i){
                            let userObj = arrayUsers.find(item => item.id === i);
                            nameInput.textContent = `${userObj.name}`;
                            address.textContent = `${userObj.email}`;
                            email.textContent = `${userObj.website}`;
                        }
                    }

                });
            }
        })
        .catch(function (error) {
            console.log(error);
        })
        .finally(function () {
            console.log('Posts has been completed')
        });
}

class Ajax {
    static get(url){
        return new Promise(function (resolve) {
            const  request = new XMLHttpRequest();
            request.open('GET', url);
            request.onload = function () {
                const result = JSON.parse(request.response);
                resolve(result)
            };
            request.send();
            request.onerror = function () {
                console.log(error)
            }
        })
    }
}

const arrayUsers = [];
const arrayPosts = [];


function getUsers() {
    Ajax
        .get('https://jsonplaceholder.typicode.com/users')
        .then(function (data) {
           data.forEach(item => {
              let users = {
                   id:item.id,
                   name:item.name,
                   email:item.email,
                   website:item.website
               };
               arrayUsers.push(users);
           })
        })
        .catch(function (error) {
            console.log(error);
        })
        .finally(function () {
            console.log('Users has been completed')
        })
}



function getPost() {
    Ajax
        .get('https://jsonplaceholder.typicode.com/posts')
        .then(function (data) {
            data.forEach(item => {
                let post = {
                    userId: item.userId,
                    postId: item.id,
                    title: item.title,
                    body: item.body
                };
                arrayPosts.push(post);
            })
        })
        .catch(function (error) {
            console.log(error);
        })
        .finally(function () {
            console.log('Posts has been completed')
        })
}

let myPix = new Array('images/images1.jpeg','images/images2.jpeg','images/images3.jpeg','images/images4.jpeg','images/images5.jpeg','images/images6.jpeg','images/images7.jpeg','images/images8.jpeg','images/images9.jpeg','images/images10.jpeg');

function choosePic() {
    let randomNum = Math.floor(Math.random() * myPix.length);
    return myPix[randomNum];
}

