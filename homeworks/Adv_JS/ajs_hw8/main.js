const btn = document.querySelector('.btn');
const dataLocation = document.querySelector('.location');

async function getIp() {
     const response = await fetch(`https://api.ipify.org/?format=json`);
     const result = await response.json();
     const ip = result.ip;

     async function getLocation() {
         const response = await fetch(`http://ip-api.com/json/${ip}?lang=ru&fields=continent,country,region,city,district`);
         const result = await response.json();
         const li = document.createElement('li');
         const ul = document.createElement('ul');
         dataLocation.append(ul);
         ul.append(li);
         li.innerHTML = 'Континент: ' + `${result.continent}` + `<br>` +
             'Cтрана: ' + `${result.country}` +  `<br>` +
             'Регион: ' + `${result.region}` +  `<br>` +
             'Город: ' + `${result.city}` +  `<br>` +
             'Район: ' + `${result.district}` +  `<br>`;
     }
     getLocation()
}

btn.addEventListener('click',function () {
    getIp();
    dataLocation.classList.add('active');
});
