
let form = document.querySelector('.password-form'),
    pass = form.querySelectorAll('.password'),
    message = form.querySelector('.mess');

form.addEventListener('submit', function (e) {
    let error = (pass[0].value !== pass[1].value);
    if (error){
        message.classList.add('hidden');
    }else {
        alert ('You are welcome!');
        form.reset();
    }
    error && e.preventDefault();
}, false);

let eyes = document.querySelectorAll('.icon-password');
eyes.forEach(eye => {
    eye.addEventListener('click', function (event) {
        let target = event.target;
        let input = target.previousElementSibling;
        if (input.type === "password") {
            input.type = "text";
            target.classList.toggle('fa-eye-slash');
            target.classList.toggle('fa-eye');
        } else {
            input.type = "password";
            target.classList.toggle('fa-eye-slash');
            target.classList.toggle('fa-eye');
        }
    })
});

form.addEventListener('click', function () {
        message.classList.remove('hidden');
    });