
function changeColor (e) {
 const key = document.querySelector(`.key[data-key="${e.keyCode}"]`)
    if (key) {
        key.classList.add('press');
    }
}
window.addEventListener('keydown', changeColor);

function removeTransition(e) {
 if (e.propertyName !== "transform") return;
    this.classList.remove('press');
}

const keys = document.querySelectorAll('.key');
keys.forEach(key => key.addEventListener('transitionend', removeTransition));


// // 1. Почему для работы с input не рекомендуется использовать события клавиатуры?
// События клавиатуры отражают только физическое нажатие клавиш (keydown, keyup, kepress),
// этого не достаточно для работы с input, так как нам не нужен факт нажатия клавиш.
// По-этому для работы с input лучше использовать другие обработчики событий, такие как (focus change input blur).
