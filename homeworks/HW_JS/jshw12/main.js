 let images = [];
 let i = 0;
 let time;

images[0] = './images/img1.jpg';
images[1] = './images/img2.jpg';
images[2] = './images/img3.jpg';
images[3] = './images/img4.png';

window.onload = changeImage;

function changeImage(){
    document.slide.src = images[i];
    if (i < images.length - 1){
        i++;
    } else {
        i = 0;
    }
    time = setTimeout(changeImage, 10000);
}

document.querySelector('.stop').onclick = function () {
     clearTimeout(time);
 };

 document.querySelector('.move').onclick = function () {
     setTimeout(changeImage,10000);
 };

 // 1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.
 //     `setTimeout()` - запустит функцию 1 раз через указанный интервал времени.
 //     `setInterval()` - запустит одну и туже функцию много раз через указанный интервал времени.
 //
 // 2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?
 //     Нет мгновенно `setTimeout()` 0 не сработает,сначала отработает текущий код а затем функция запущенная  `setTimeout()`0.
 //
 // 3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?
 //     Функция `clearInterval()` отменяет ранее запуценную функию `setInterval()`, соотведственно если ранее созданный
 // цыкл нам не нужен другого варианта остановить цыкл кроме как `clearInterval()`нет.