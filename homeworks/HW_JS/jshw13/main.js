
let head = document.head,
    link = document.createElement('link');
link.rel = 'stylesheet';
if(localStorage.getItem('themeStyle') === 'blackstyle'){
    link.href = 'blackstyle.css';
    document.querySelector('.change').setAttribute('checked', true);
} else {
    link.href = 'whitestyle.css';
}

head.appendChild(link);

document.querySelector('.change').addEventListener('change', (e) => {
    let btn = e.target;
    if (btn.checked) {
        link.href = "blackstyle.css";
        localStorage.setItem('themeStyle', 'blackstyle');
    } else {
        link.href = "whitestyle.css";
        localStorage.setItem('themeStyle', 'whitestyle');
    }
});
