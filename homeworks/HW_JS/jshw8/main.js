
let fieldInput = document.querySelector('.input');

let correctMessage = document.querySelector('.add');

let cross = document.querySelector('.button');

let inCorrectMessage = document.querySelector('.error');


function removeMessage() {
    let cross = document.querySelector('.button');
    let correctMessage = document.querySelector('.add');
    let fieldInput = document.querySelector('.input');
    cross.classList.remove('plusButton');
    correctMessage .classList.remove('plusAdd');
    fieldInput.value = '';
}

document.querySelector('.input').addEventListener('focus', function addBorder() {
    let fieldInput = document.querySelector('.input');
    let inCorrectMessage = document.querySelector('.error');
    removeMessage();
    fieldInput.classList.add('inpBorder');
    inCorrectMessage.innerHTML = '';
    fieldInput.classList.remove('invalid');
});

cross.addEventListener('click', function() {
    removeMessage();
});

fieldInput. addEventListener('blur', function () {
    let data = document.querySelector('.input').value;
    if (fieldInput.value > 0  && fieldInput.value) {
        correctMessage .classList.add('plusAdd');
        fieldInput.classList.add('correct');
        cross.classList.add('plusButton');
        inCorrectMessage.classList.remove('invalid');
        fieldInput.classList.remove('invalid');
        correctMessage .innerHTML = "The price is" + "   " + data + "   $";

    } else {
        fieldInput.classList.add('invalid');
        inCorrectMessage.classList.add('invalid');
        inCorrectMessage.innerHTML = 'Please enter correct price.';
    }
});


// 1. Опишите своими словами, как Вы понимаете, что такое обработчик событий.
// 2. Обработчик событий - это функция которая отработаеться если событие произошло.
//     Обработчик можно назначить 3-мя способами, напрямую в html - приписав событию (on)click = "",
//     или через html+ JS где в js пишется функция а и передаеться в html (on)click = "myFunction".
//     При таких способах на 1 и тот же элемент разместить несколько событий невозможно.
//     По этому универсальный обработчик событий addEventListener который пишется только в JS часто испольузется.