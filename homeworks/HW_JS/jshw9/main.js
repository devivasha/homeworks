let tabs = Array.from(document.querySelectorAll('.tabs li'));
let tabsDescr = Array.from(document.querySelectorAll('.tabs-content li'));


document.querySelector('.tabs').addEventListener('click', (event) => {
    let target = event.target;
    const lastActive = document.querySelector('.active');
    lastActive ? lastActive.classList.remove('active') : false;
    target.classList.add('active');
    target.classList.remove('tabs-title2');
    let index = target.getAttribute('data-tab');
    document.querySelector('.active2').remove('active2');
    tabsDescr[index].classList.add('active2');
});
