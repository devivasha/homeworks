import React, {Component} from 'react';
import Button from "./Button";
import Modal from './Modal';

class App extends Component {
    state = {
        activeModal: '',
        showModal: false
    };
    toggleModal = (modalName) => {
        this.setState({
            showModal: ! this.state.showModal,
            activeModal: modalName,
        })
    };
    render() {
        const modals= [
            {
                name: 'modal1',
                header: 'Do you want to delete this file?',
                text: 'Once you delete this file, if won`t be to undo this action. Are you sure you want to delete it?',
                closeButton:'block'
            },
            {
                name: 'modal2',
                header: 'Do you want to add this file?',
                text: 'Once you add this file, if won`t be to undo this action. Are you sure you want to add it?',
                closeButton:'none'
            }
            ];
        const {showModal} = this.state;
        const activeModalObject = modals.find(m => m.name === this.state.activeModal);
        const activeModal = <Modal {...activeModalObject} toggleModal={() => this.toggleModal(activeModalObject.name)} />;
        return (
            <div className="App" onClick={()=> {this.setState({showModal: ! this.state.showModal})}}>
                <div>{[ {text:'Open first modal', color:'lightblue', modalName:'modal1'},
                        {text:'Open second modal', color:'grey', modalName:'modal2'}].map((button, i) =>
                        <Button key={i}
                                doSomething={() => {this.toggleModal(button.modalName)}}
                                color={button.color}
                                text={button.text}/>
                    )}
                    {showModal && activeModal}
                </div>
            </div>
        );
    }
}

export default App;