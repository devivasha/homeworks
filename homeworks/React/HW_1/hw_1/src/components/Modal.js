import React, {Component} from 'react';
import PropTypes from "prop-types";
import Button from "./Button";


class Modal extends Component {
    render() {
        const {header, closeButton, text, toggleModal} = this.props;
        return (
            <div>
            <div className="modal my-class" id="exampleModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">{header}</h5>
                            <button style={{display:closeButton}} onClick={toggleModal} type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {text}
                        </div>
                        <div className="modal-footer">
                            {[  {actions:'Ok', color:'lightblue'},
                                {actions:'Cancel', color:'grey'}].map((button, i) =>
                                <Button key={i}
                                        onClick={()=> {this.setState({showModal: ! this.state.showModal})}}
                                        color={button.color}
                                        text={button.actions}/>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>)
    }
}
Modal.propsTypes={
    header: PropTypes.string,
    closeButton:PropTypes.boolean,
    text: PropTypes.string,
    actions: PropTypes.string,
    toggleModal:PropTypes.func
};
export default Modal;
