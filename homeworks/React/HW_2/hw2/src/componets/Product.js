import React, {Component} from 'react';
import Button from "./Button";
import Star from "./Star";
import Modal from "./Modal";

class Product extends Component {
    state = {
        activeModal: '',
        showModal: false
    };
    toggleModal = (modalName) => {
        this.setState({
            showModal: ! this.state.showModal,
            activeModal: modalName
        })
    };
    render() {
        const modals = [
            {
                name: 'modal1',
                header: 'Do you want to add the product into the cart?',
                text: 'Once you added this product, it won`t be to undo this action. Are you sure you want to add it?',
                closeButton:'none'
            }
        ];
        const {productItem} = this.props;
        const {showModal} = this.state;
        const activeModalObject = modals.find(m => m.name === this.state.activeModal);
        const activeModal = <Modal {...activeModalObject} toggleModal={() => this.toggleModal(activeModalObject.name)} />;
        return (
            <div className="d-inline-block" id={productItem.id}>
                <div className="card my-card">
                    <div className="card-body">
                        <img className='img' src={productItem.url} alt=""/>
                        <h5 className="card-title">{productItem.title}</h5>
                        <p className="card-text">{productItem.color}</p>
                        <p className="card-text">{productItem.price}</p>
                        <div>
                                <Button doSomething={() => {this.toggleModal('modal1')}}
                                        color='green'
                                        text='ADD TO CART'/>
                            {showModal && activeModal}
                            <div className='starPosition'>
                                <Star product={productItem}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Product;