import React, {Component} from 'react';
import PostProducts from "./PostProducts";
import {Route, Switch } from 'react-router-dom';
import Tollbar from './Tollbar'
import Basket from "./Basket";
import Favourites from "./Favourites";

class App extends Component {
    render() {
        return (
            <div className="App">
                <Tollbar/>
                <div className="content">
                    <Switch>
                        <Route exact path="/" component={() => <PostProducts />} />
                        <Route exact path="/basket" component={() => <Basket />} />
                        <Route exact path="/favourites" component={() => <Favourites />} />
                    </Switch>
                </div>

            </div>
        );
    }
}

export default App;