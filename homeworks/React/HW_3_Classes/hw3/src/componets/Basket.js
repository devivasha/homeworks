import React, {Component} from 'react';
import Product from "./Product";

class Basket extends Component {
    render() {
        const buyItems =  JSON.parse(localStorage.getItem('bought'));
        return (
            <div>
                <h1>Basket</h1>
                { buyItems.length ?
                    buyItems.map((product, i) => {
                        return <Product key={i} productItem={product}/>
                    }) : null
                }
            </div>
        );
    }
}

export default Basket;