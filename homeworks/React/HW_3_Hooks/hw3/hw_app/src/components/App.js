import React from 'react';
import {Route, Switch } from 'react-router-dom';
import ProductsList from "./ProductsList";
import Basket from './Basket';
import Favourites from './Favourites'
import Tollbar from "./Tollbar";

const App =()=> {
    return (
        <div>
            <Tollbar/>
            <div className="content">
                <Switch>
                    <Route exact path="/" component={() => <ProductsList />} />
                    <Route path="/basket" component={() => <Basket />} />
                    <Route path="/favourites" component={() => <Favourites />} />
                </Switch>
            </div>

        </div>
    );
}
export default App