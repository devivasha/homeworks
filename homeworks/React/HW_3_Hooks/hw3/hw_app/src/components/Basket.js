import React from 'react';
import Product from './Product';

const Basket =()=>{
    const buyItems =  JSON.parse(localStorage.getItem('bought'));
    return (
        <div>
            <h1>Basket</h1>
            {buyItems.map((product, i) => {
                    return <Product key={i} product={product}/>
                })}
        </div>
    );
}
export default Basket;