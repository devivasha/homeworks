import React from 'react';

const Button =(props)=>{
    return (
            <div className="d-inline">
                <button style={{background:props.color}}
                        onClick={props.onTestClick}
                        className="btn m-2"> {props.actions}
                </button>
            </div>
        );
}
export default Button;

