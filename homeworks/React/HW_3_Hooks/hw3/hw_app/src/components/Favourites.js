import React from 'react';
import Product from './Product';

const Favourites =()=>{
        const starItems =  JSON.parse(localStorage.getItem('liked'));
        return (
            <div>
                <h1>Favourites</h1>
                {starItems.map((product, i) => {
                        return <Product key={i} product={product}/>
                    })}
            </div>
        );
}
export default Favourites;