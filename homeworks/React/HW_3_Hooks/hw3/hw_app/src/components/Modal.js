import React, {forwardRef, useState, useImperativeHandle, useRef} from 'react';
import ReactDOM from 'react-dom'
import Button from "./Button";



const Modal = forwardRef((props, ref) => {
    const [display, setDisplay] = useState(false);

    useImperativeHandle(ref,() => {
        return{
            openModal: () => open(),
            closeModal: () => close()
        }
    } );
    const open = () => {
        setDisplay(true)
    };
    const close = () => {
        setDisplay(false)
    };
    const buttons = [
        {actions:'ADD TO CART', color:'green', onTestClick: props.toggleProduct},
        {actions:'REMOVE FROM CART', color:'lightblue',onTestClick: props.toggleProduct}
    ];

    if (display) {
        return ReactDOM.createPortal(<div className={'modal-wrapper'}>
            <div onClick={close} className={'modal-backdrop'}>
                <div className={'modal-box'}>
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">{'Do you want to add the product into the cart?'}</h5>
                            <button onClick={close} style={{display:"block"}} type="button" className="close" data-dismiss="modal"
                                     aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">{'Once you added this product, it won`t be to undo this action. Are you sure you want to add it?'}</div>
                        <div className="modal-footer">
                            {buttons.map((button, i) =>
                                <Button key={i} {...button}/>
                            )}
                        </div>
                    </div>


                </div>
            </div>
        </div>, document.getElementById('modal-root'));
    }
    return null;
});

export default Modal;
