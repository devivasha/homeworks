import React, {useRef} from 'react';
import Button from "./Button";
import Modal from "./Modal";

const Product = (props)=> {
    const modalRef = useRef();

    const openModal = () => {
        modalRef.current.openModal()
    };
    const onLikedClick =(product)=> {
        product.liked = false;
        let storage = JSON.parse(localStorage.getItem('liked'));
        const findProduct = storage.find(item => item.id === product.id);
        const indexProduct = storage.indexOf(findProduct);
        if(findProduct) {
            storage.splice(indexProduct, 1);
            return localStorage.setItem('liked', JSON.stringify(storage));
        } else {
            product.liked = true;
            storage.push(product);
            localStorage.setItem('liked', JSON.stringify(storage));
        }
    };

    const onBuyClick = (product)=> {
        product.bought = false;
        let storage2 = JSON.parse(localStorage.getItem('bought'));
        const findProduct2 = storage2.find(item => item.id === product.id);
        const indexProduct2 = storage2.indexOf(findProduct2);
        if(findProduct2) {
            storage2.splice(indexProduct2, 1);
            return localStorage.setItem('bought', JSON.stringify(storage2));
        } else {
            product.bought = true;
            storage2.push(product);
            localStorage.setItem('bought', JSON.stringify(storage2));
        }
    };

    return (
            <div className="d-inline-block" >
                <div className="card my-card">
                    <div className="card-body">
                        <div id={props.product}>
                            <img className='img' src={props.product.url} alt=""/>
                            <h5 className="card-title">{props.product.title}</h5>
                            <p className="card-text">{props.product.color}</p>
                            <p className="card-text">{props.product.price}</p>
                            <div className='starPosition'>
                                <i id={props.product.id} onClick={()=> {
                                    onLikedClick(props.product);
                                }} className={(props.product.liked === true) ? 'fa fa-star' : 'fa fa-star-o'} aria-hidden="true"> </i>
                            </div>
                            <Button onTestClick={openModal} color={(props.product.bought === true) ? 'lightblue' : 'green'} actions={(props.product.bought === true) ? 'REMOVE FROM CART' : 'ADD TO CART'}/>
                                <Modal ref={modalRef} toggleProduct={()=> onBuyClick(props.product)}/>
                        </div>
                    </div>
                </div>
            </div>
        );
}
export default Product;

