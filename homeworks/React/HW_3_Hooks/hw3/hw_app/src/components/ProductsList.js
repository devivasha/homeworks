import React, { useEffect, useState} from 'react';
import Product from './Product';
import axios from 'axios';


const ProductsList = () => {
    const [data, setData] = useState([]);
    useEffect(() => {
        const fetchProducts = async () => {
            const response = await axios.get ('./data.json');
            setData(response.data);
        };
        fetchProducts();
    }, []);

    const likedProducts = JSON.parse(localStorage.getItem('liked'));
    const buyProducts = JSON.parse(localStorage.getItem('bought'));

    const checkStorage = ()=> {
        let storageBuy = localStorage.getItem('bought');
        let storageLike = localStorage.getItem('liked');
        if (!storageBuy){
            localStorage.setItem('bought', JSON.stringify([]));
        }
        if (!storageLike) {
            localStorage.setItem('liked', JSON.stringify([]))
        }
    };
    useEffect(()=>{
       checkStorage();
    },[]);

    return (
    <div className="App">
        <main>
            <h1>LATEST ARRIVALS</h1>
            {data.map((product, i) => {
                const findProduct = likedProducts.find(item => item.id === product.id);
                const findProduct2 = buyProducts.find(item => item.id === product.id);
                return <Product product={findProduct ? findProduct: product && findProduct2 ? findProduct2: product} key={i}/>
            })}
        </main>
    </div>
    );
};

export default ProductsList;