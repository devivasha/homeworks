import React, {useState} from 'react';
import Product from './Product';

const Basket =()=>{
    const [buy, setBuy]= useState(JSON.parse(localStorage.getItem('bought')));
    const onUnBuyHandler = (product)=>{
        const index = buy.findIndex(item => item.id ===  product.id);
        if (index >= 0){
            const result = [...buy];
            result.splice(index, 1);
            setBuy(result);
        }
    };
    return (
        <div>
            <h1>Basket</h1>
            {buy.map((product, i) => {
                const findProduct2 = buy.find(item => item.id === product.id);
                    return <Product onUnBuyClick={onUnBuyHandler} key={i} product={findProduct2 ? findProduct2: product}/>
                })}
        </div>
    );
}
export default Basket;