import React, {forwardRef, useState, useImperativeHandle} from 'react';
import ReactDOM from 'react-dom'
import Button from "./Button";



const Modal = forwardRef((props, ref) => {
    console.log(props);

    const [display, setDisplay] = useState(false);

    useImperativeHandle(ref,() => {
        return{
            openModal: () => open(),
            closeModal: () => close()
        }
    } );
    const open = () => {
        setDisplay(true)
    };
    const close = () => {
        setDisplay(false)
    };

    if (display) {
        return ReactDOM.createPortal(
            <div className={'modal-wrapper'}>
            <div onClick={close} className={'modal-backdrop'}>
                <div className={'modal-box'}>
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">{props.header}</h5>
                            <Button actions={'X'} onTestClick={close}/>
                        </div>
                        <div className="modal-body">{props.body}</div>
                        <div className="modal-footer">
                            <Button actions={props.button} color={props.color} onTestClick={props.toggleProduct}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>, document.getElementById('modal-root'));
    }
    return null;
});

export default Modal;
