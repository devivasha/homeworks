import {combineReducers} from "redux";
import products from './products';
import modal from './modal';

const rootReducer = combineReducers({
    products,
    modal
});
export default rootReducer;