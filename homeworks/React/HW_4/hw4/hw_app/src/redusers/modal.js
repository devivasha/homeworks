import { OPEN_PRODUCT_MODAL, CLOSE_PRODUCT_MODAL } from '../types'

const initialState = {
    isModalOpen: false
}

export default(state = initialState, action) => {
    switch (action.type) {
        case OPEN_PRODUCT_MODAL:
            return {
                ...state,
                isModalOpen: true
            };
        case CLOSE_PRODUCT_MODAL:
            return {
                 ...state,
                isModalOpen: false
            };
        default:
            return state;

    }
}