import React, {useState} from 'react';
import Product from './Product';
import OrderForm from "./OrderForm";

const Basket =()=>{

    const [buy, setBuy]= useState(JSON.parse(localStorage.getItem('bought')));

    const onUnBuyHandler = (product)=>{
        const index = buy.findIndex(item => item.id ===  product.id);
        if (index >= 0){
            const result = [...buy];
            result.splice(index, 1);
            setBuy(result);
        }
    };
    const update =(data)=>{
        console.log('UPDATE');
        let itemsBought = localStorage.getItem('bought');
            setBuy({itemsBought});
        return itemsBought = localStorage.setItem('bought', JSON.stringify([]));
    };
    return (
        <div>
            <h1>Basket</h1>
            { buy.length > 0 ?
                buy.map((product, i) => {
                return <Product onUnBuyClick={onUnBuyHandler} key={i} product={product}/>
                }):null
            }
             <div className={'orderForm'}>
                 <h2 className={'h2'}>Order Form</h2>
                 <OrderForm updateData={(val)=>{update(val)}} />
             </div>
        </div>
    );
}
export default Basket;