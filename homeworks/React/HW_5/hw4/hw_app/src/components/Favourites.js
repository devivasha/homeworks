import React, {useState} from 'react';
import Product from './Product';

const Favourites =()=>{
    const [fav, setFav]= useState(JSON.parse(localStorage.getItem('liked')));
    const onUnLikeHandler = (product)=>{
        const index = fav.findIndex(item => item.id ===  product.id);
        if (index >= 0){
            const result = [...fav];
            result.splice(index, 1);
            setFav(result);
        }
    };
    return (
            <div>
                <h1>Favourites</h1>
                {fav.map((product, i) => {
                        return <Product onUnLikeClick={onUnLikeHandler} key={i} product={product}/>
                    })}
            </div>
        );
};
export default Favourites;