import React from 'react';
import {Field, reduxForm , SubmissionError} from 'redux-form';
import Button from './Button'


const submit=({ firstName='', familyName='', age='', address='', phone='' })=>{
    let error = {};
    let isError = false;

    if(firstName.trim() === ''){
        error.firstName = 'Required';
        isError = true;
    }
    if(familyName.trim() === ''){
        error.familyName = 'Required';
        isError = true;
    }
    if(age.trim() === ''){
        error.age= 'Required';
        isError = true;
    }
    if(address.trim() === ''){
        error.address = 'Required';
        isError = true;
    }
    if(phone.trim() === ''){
        error.phone = 'Required';
        isError = true;
    }
    if(isError){
        throw new SubmissionError(error)
    } else {
        let itemsBought = JSON.parse(localStorage.getItem('bought'));
        let info = {firstName, familyName, age, address, phone};
        console.log(itemsBought, info);
    }
};

const renderField =({label, type, input, meta: { touched, error }})=> (
    <div className="input-row">
        <label>{label}</label>
        <br/>
        <input {...input} type={type}/>
        {touched && error &&
        <span className="error">{error}</span>
        }
    </div>
);

const OrderFormFunc =({handleSubmit, updateData})=> {
    const passedData = JSON.parse(localStorage.getItem('bought'));
    const handleClick = () => updateData(passedData);

   return (
       <div>
        <form className="container" onSubmit={handleSubmit(submit)}>
            <Field name="firstName" label="First Name" component={renderField} type="text" value=''/>
            <Field name="familyName" label="Family Name" component={renderField} type="text"/>
            <Field name="age" label="Age" component={renderField} type="number"/>
            <Field name="address" label="Address" component={renderField} type="text"/>
            <Field name="phone" label="Phone" component={renderField} type="number"/>
            <Button actions={'Checkout'} type={'submit'} color={'green'} onTestClick={handleClick}/>
        </form>
    </div>
   )
};
const OrderForm = reduxForm({
    form: 'order'
})(OrderFormFunc);

export default OrderForm;

