import React, {useEffect, useRef, useState} from 'react';
import Button from "./Button";
import Modal from "./Modal";
import { connect} from "react-redux";
import {OPEN_PRODUCT_MODAL, CLOSE_PRODUCT_MODAL} from "../types";

const Product = (props)=> {

    const modalRef = useRef();

    const openModal = () => {
        modalRef.current.openModal()
    };

    const [isLiked, setIsLiked] = useState([false]);
    const setStar = (status) => {
        setIsLiked({isLiked: status})
    };

    const [isBought, setIsBought] = useState([false]);
    const setBought = (status) => {
        setIsBought({isBought: status})
    };

 const {onUnLikeClick=()=>{}, product} = props;
 const {onUnBuyClick=()=>{}} = props;

    useEffect(()=>{
        setStar(props.product.liked);
        setBought(props.product.bought)
    },[props.product.liked, props.product.bought]);


    const onLikedClick =(product)=> {
        product.liked = false;
        let storage = JSON.parse(localStorage.getItem('liked'));
        const findProduct = storage.find(item => item.id === product.id);
        const indexProduct = storage.indexOf(findProduct);
        if(findProduct) {
            storage.splice(indexProduct, 1);
            return localStorage.setItem('liked', JSON.stringify(storage));
        } else {
            product.liked = true;
            storage.push(product);
            localStorage.setItem('liked', JSON.stringify(storage));
        }
    };
    const onBuyClick = (product)=> {
        product.bought = false;
        let storage2 = JSON.parse(localStorage.getItem('bought'));
        const findProduct2 = storage2.find(item => item.id === product.id);
        const indexProduct2 = storage2.indexOf(findProduct2);
        if(findProduct2) {
            storage2.splice(indexProduct2, 1);
            return localStorage.setItem('bought', JSON.stringify(storage2));
        } else {
            product.bought = true;
            storage2.push(product);
            localStorage.setItem('bought', JSON.stringify(storage2));
        }
    };
    return (
            <div className="d-inline-block" >
                <div className="card my-card">
                    <div className="card-body">
                        <div id={product.id}>
                            <img className='img' src={product.url} alt=""/>
                            <h5 className="card-title">{product.title}</h5>
                            <p className="card-text">{product.color}</p>
                            <p className="card-text">{product.price}</p>
                            <div className='starPosition'>
                                <i id={product.id}
                                   onClick={()=> {
                                       onLikedClick(product);
                                       setStar(isLiked);
                                       onUnLikeClick(product);
                                   }} className={(product.liked === true) ? 'fa fa-star' : 'fa fa-star-o'} aria-hidden="true"> </i>
                            </div>
                            <Button onTestClick={openModal} color={(product.bought === true) ? 'white' : 'green'} actions={(product.bought === true) ? 'X' : 'ADD TO CART'}/>
                            <Modal  ref={modalRef} toggleProduct={()=>{
                                onBuyClick(product);
                                setBought(isBought);
                                onUnBuyClick(product);}}
                                    header={(product.bought === true) ? 'Do you want to remove the product from the cart?' : 'Do you want to add the product into the cart?'}
                                    body={(product.bought === true) ? 'Are you sure you want to remove it?':'Are you sure you want to add it?'}
                                    button={(product.bought === true) ? 'REMOVE':'ADD'}
                                    color={(product.bought === true) ? 'grey':'green'}/>
                        </div>
                    </div>
                </div>
            </div>
        );
};

function mapStateToProps(state) {
    return {
        isModalOpen: state.modal.isModalOpen
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        openProductModal: ()=> dispatch({
            type: OPEN_PRODUCT_MODAL
        }),
        closeProductModal: ()=> dispatch({
            type: CLOSE_PRODUCT_MODAL
        })
    }
};

export default  connect(
    mapStateToProps,
    mapDispatchToProps
) (Product);

