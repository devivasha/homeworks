import React, { useEffect} from 'react';
import Product from './Product';
import {connect} from "react-redux";
import {fetchProducts} from "../actions";


const ProductsList = ({fetchProducts, products}) => {

    useEffect(() => {
        fetchProducts();
    },[fetchProducts]);

    const likedProducts = JSON.parse(localStorage.getItem('liked'));
    const buyProducts = JSON.parse(localStorage.getItem('bought'));

    const checkStorage = ()=> {
        let storageBuy = localStorage.getItem('bought');
        let storageLike = localStorage.getItem('liked');
        if (!storageBuy){
            localStorage.setItem('bought', JSON.stringify([]));
        }
        if (!storageLike) {
            localStorage.setItem('liked', JSON.stringify([]))
        }
    };
    useEffect(()=>{
       checkStorage();
    },[]);

    return (
    <div className="App">
        <main>
            <h1>LATEST ARRIVALS</h1>
            {products.map((product, i) => {
                const findProduct = likedProducts.find(item => item.id === product.id);
                const findProduct2 = buyProducts.find(item => item.id === product.id);
                return <Product product={findProduct ? findProduct: product && findProduct2 ? findProduct2: product} key={i}/>

            })}
        </main>
    </div>
    );
};

function mapStateToProps(state) {
    return {
        products: state.products
    }
}

const mapDispatchToProps = {fetchProducts};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductsList);