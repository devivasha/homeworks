import React from 'react';
import {Link} from 'react-router-dom';

const Tollbar =()=>{
    return (
        <div>
            <nav className="navbar navbar-light" style={{backgroundColor: "#e3f2fd"}}>
                <ul className="navbar-nav" style={{display:"inline-block"}}>
                    <li className="nav-item" style={{display:"inline-block", margin:'10px'}}><Link to="/">Home</Link></li>
                    <li className="nav-item" style={{display:"inline-block", margin:'10px'}}><Link to="/basket">Basket</Link></li>
                    <li className="nav-item" style={{display:"inline-block", margin:'10px'}}><Link to="/favourites">Favourites</Link></li>
                </ul>
            </nav>
        </div>
    );
}
export default Tollbar