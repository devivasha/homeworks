import {combineReducers} from "redux";
import products from './products';
import modal from './modal';
import {reducer as formReducer} from 'redux-form';

const rootReducer = combineReducers({
    products,
    modal,
    form: formReducer
});
export default rootReducer;